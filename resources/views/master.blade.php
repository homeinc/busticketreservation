<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @vite('resources/css/app.css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <title>Bus Ticket Reservation</title>
</head>
<body>
    @include('partials.navbar')
    <div class="container mx-auto px-4">
        @yield('body')
    </div>
    @yield('javascript')
</body>
</html>
