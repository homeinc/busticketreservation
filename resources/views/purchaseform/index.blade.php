@extends('master')

@section('body')
        <div class="min-h-screen p-6 bg-gray-100 flex">
            <div class="container max-w-screen-lg mx-auto">
                @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                <h1 class="flex justify-center text-2xl font-semibold ">
                    Ticket Purchase
                </h1>

                    <form method="POST">
                        @csrf
                    <div class="bg-white rounded shadow-lg p-4 px-4 md:p-8 mb-6 mt-5">
                        <div class="grid gap-4 gap-y-2 text-sm grid-cols-1 lg:grid-cols-3">
                            <div class="text-gray-600">
                                <div>
                                    <h2>Ticket Details</h2>
                                </div>
                                <h3 class="text-lg">
                                   Company : {{ $ticket['company'] }}
                                </h3>
                                 <div>
                                     Departure: {{$ticket['departure_city_name']}}
                                 </div>
                                <div>
                                    Arrival: {{$ticket['arrival_city_name']}}
                                </div>
                                <div>
                                    Departure Date: {{$ticket['departure_date']}}
                                </div>
                                <div>
                                    Arrival Date: {{$ticket['arrival_date']}}
                                </div>
                                <div class="font-bold text-3xl">
                                    Total Cost : ${{$ticket['ticket_count']   * $ticket['default_price']}}
                                </div>
                            </div>

                            <div class="lg:col-span-2">
                                <div class="grid gap-4 gap-y-2 text-sm grid-cols-1 md:grid-cols-5">
                                    <div class="md:col-span-5">
                                        <label for="full_name">Full Name</label>
                                        <input type="text" name="full_name" id="full_name" class="h-10 border mt-1 rounded px-4 w-full bg-gray-50" value="" />
                                    </div>

                                    <div class="md:col-span-5">
                                        <label for="email">Email Address</label>
                                        <input type="text" name="email" id="email" class="h-10 border mt-1 rounded px-4 w-full bg-gray-50" value="" placeholder="email@domain.com" />
                                    </div>

                                    <div class="md:col-span-3">
                                        <label for="social_security">Social ID Number</label>
                                        <input type="text" name="social_security" id="social_security" class="h-10 border mt-1 rounded px-4 w-full bg-gray-50" value="" placeholder="11 digits" />
                                    </div>

                                    <div class="md:col-span-3">
                                        <label for="credit_card">Credit Card Number</label>
                                        <input type="text" name="credit_card" id="credit_card" class="h-10  border mt-1 rounded px-4 w-full bg-gray-50" value="" placeholder="0000-00000-0000-0000" />
                                    </div>
                                    <div class="md:col-span-3">
                                        <label for="expire_date">Expire Date</label>
                                        <input type="text" name="expire_date" id="expire_date" class="h-10 border mt-1 rounded px-4 w-full bg-gray-50" value="" placeholder="" />
                                    </div>

                                    <div class="md:col-span-2">
                                        <label for="ccv">CCV</label>
                                        <input type="text" name="ccv" id="city" class="h-10 border mt-1 rounded px-4 w-full bg-gray-50" value="" placeholder="" />
                                    </div>

                                    <div class="md:col-span-5">
                                        <div class="inline-flex items-center">
                                            <input type="checkbox" name="billing_same" id="billing_same" class="form-checkbox" />
                                            <label for="billing_same" class="ml-2">My billing address is different than above.</label>
                                        </div>
                                    </div>

                                    <div class="md:col-span-5 text-right">
                                        <div class="inline-flex items-end">
                                            <button class="bg-orange-600 hover:bg-orange-700 text-white font-bold py-2 px-4 rounded" name="cancel_form">Cancel</button>
                                        </div>
                                        <div class="inline-flex items-end">
                                            <button class="bg-blue-600 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" type="submit">Submit</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        </div>
                    </form>
                </div>
        </div>

@endsection

@section('scripts')

@endsection

