@extends('master')

@section('body')
    @foreach($tours as $tour)
            <div class="bg-white  mx-auto w-3/5 p-4">
                <div class="bg-gray-50 shadow-lg mx-auto  p-2">
                    <form action="{{route('purchase.store')}}" method="POST">
                        @csrf
                        <img class="logo rounded flex flex-nowrap" src="{{$tour->logo}}" alt="logo"/>
                        <div class="text-lg font-semibold">
                            {{ $tour->company }}
                        </div>
                        <div class="grid grid-cols-3">
                            <div>
                                <label for="departure_city_name">Departure Station</label>
                                <input type="text" class="border-0 bg-gray-100 focus:outline-none" id="departure_city_name" readonly name="departure_city_name"
                                       value="{{ ucfirst($tour['departure_station']) }}">
                                <label for="arrival_city_name">Arrival Station</label>
                                <input type="text" id="arrival_city_name" readonly name="arrival_city_name"
                                       value="{{ ucfirst($tour['arrival_station']) }}">
                                <label for="departure_date">Departure Date</label>
                                <input type="text" id="departure_date" readonly name="departure_date"
                                       value="{{ $tour['departure_date'] }}">
                                <label for="arrival_date">Arrival Date</label>
                                <input type="text" id="arrival_date" readonly name="arrival_date"
                                       value="{{ $tour['arrival_date'] }}">

                            </div>
                            <div>
                                <div>
                                    <div class="text-gray-600 font-semibold">Distance:</div>
                                    {{ $tour['distance'] }} km
                                </div>
                                <div>
                                    <div class="text-gray-600 font-semibold">Duration:</div>
                                    <div>{{ $tour['hours'] }}h {{ $tour['minutes'] }}m</div>
                                </div>
                            </div>
                            <div>
                                <div class="inline">
                                    <div class="text-gray-600 ">Price:</div>
                                    <div class="text-lg shadow-amber-200 mb-4">${{ number_format($tour['price'] / 100 ,2 )}}</div>
                                </div>
                                <div class="inline">
                                    <label for="ticket_count">Ticket(s)</label>
                                    <input type="text" id="ticket_count" readonly name="ticket_count"
                                           value="{{ $ticket_count }}">
                                </div>
                                <div class="inline">
                                    <div class="text-gray-600 font-semibold mt-4">Total : </div>
                                    <div class="text-3xl shadow-amber-200 mb-4">${{ number_format($tour['price'] / 100 ,2 ) * $ticket_count }}  </div>
                                </div>

                                <div>
                                  @auth
                                        <button
                                        class="mt-4 bg-blue-800 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded"
                                        type="submit">
                                        Purchase
                                        </button>
                                @endauth
                             <div>
                        </div>
                    </div>
             </div>
            </div>
                <input type="hidden" name="tour_id" value="{{$tour['tour_id']}}">
                </form>
            </div>
            </div>
    @endforeach
@endsection
