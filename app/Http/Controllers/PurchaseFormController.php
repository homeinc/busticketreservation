<?php

namespace App\Http\Controllers;

use App\Http\Requests\PurchaseFormRequest;

class PurchaseFormController extends Controller
{
    public function index()
    {

//       return redirect()->route('purchase.index');
        $ticket = session('tickets');
        return view('purchaseform.index')->with('ticket', $ticket);
    }

    public function store(PurchaseFormRequest $request)
    {
        dd($request, auth()->id());
    }
}
