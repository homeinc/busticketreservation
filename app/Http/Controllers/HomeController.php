<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Station;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $cities = City::all();
        session()->put('canNavigate', true);
        return view('homepage.index', compact('cities'));
    }
}
