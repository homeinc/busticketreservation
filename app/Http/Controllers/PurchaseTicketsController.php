<?php

namespace App\Http\Controllers;

use App\Models\Tour;
use App\Models\Trip;
use App\Models\User;
use App\services\StripeService;
use App\services\TicketService;
use Illuminate\Http\Request;
use Stripe\Exception\AuthenticationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PurchaseTicketsController extends Controller
{

    protected  int $total_amount = 0;
    public function __construct(protected TicketService $ticketService)
    {

    }
    public function store(Request $request)
    {
        $request->validate([
            'departure_city_name' => ['string', 'required'],
            'arrival_city_name' => ['string', 'required'],
            'departure_date' => ['date', 'required'],
            'arrival_date' => ['date', 'required'],
            'tour_id' => ['integer'],
            'ticket_count' => ['required','integer']
        ]);
        $tour = Tour::with('trips')->where('id', $request->tour_id)->first();
        $trip = $tour->trips->first();

        $line_items = [];
        $this->total_amount = 0;
         foreach ($this->ticketService->bookNewSeat($trip->id, $request->ticket_count) as $seat) {
            $this->total_amount += $seat->price;

             $line_items[] = [
                 'price_data' => [
                     'currency' => 'USD',
                     'product_data' => [
                         'name' => 'Seat Number :' . $seat->seat,
                     ],
                     'unit_amount' => $seat->price,
                 ],
                 'quantity' => 1
             ];
         }

        \Stripe\Stripe::setApiKey(env('STRIPE_KEY'));
        $session = \Stripe\Checkout\Session::create([
            'line_items' => $line_items,
            'mode' => 'payment',
            'success_url' => route('purchase.success', [], true) . "?session_id={CHECKOUT_SESSION_ID}",
            'cancel_url' => route('purchase.cancel'),
        ]);
         return redirect($session->url);
    }

    public function success(Request $request)
    {

        $session_id = $request->session_id;
        \Stripe\Stripe::setApiKey(env('STRIPE_KEY'));
        try {
            $session = \Stripe\Checkout\Session::retrieve($session_id);
            if (!$session)
                throw new NotFoundHttpException;
//        try {
//            $customer = \Stripe\Customer::retrieve($session->customer);
//        }
//        catch (AuthenticationException $exception) {
//            throw new AuthenticationException;
//        }
        }catch(\Exception $e) {
            throw new NotFoundHttpException;
        }
        try {
            $this->ticketService->confirmBuyingTickets($session_id);
        }catch(\InvalidArgumentException $exception) {
            return  redirect(route('home'));
        }
        return view('success');
    }

    public function cancel()
    {
        return "Cancel";
    }


    public function webhook()
    {
        $stripe = new \Stripe\StripeClient('sk_test_...');

// This is your Stripe CLI webhook secret for testing your endpoint locally.
        $endpoint_secret = env('STRIPE_WEBHOOK_SECRET');

        $payload = @file_get_contents('php://input');
        $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
        $event = null;

        try {
            $event = \Stripe\Webhook::constructEvent(
                $payload, $sig_header, $endpoint_secret
            );
        } catch(\UnexpectedValueException $e) {
            // Invalid payload
            return response('', 400);
        } catch(\Stripe\Exception\SignatureVerificationException $e) {
            // Invalid signature
            return response('', 400);
        }

// Handle the event
        switch ($event->type) {
            case 'payment_intent.succeeded':
                $paymentIntent = $event->data->object;
            break;
            case 'checkout.session.completed' :
                $session = $event->data->object;
                $session_id = $session->id;
                //                {
//                    "id": "evt_1O0JyVGFkfSXnQ2KnMKLo6Eb",
//  "object": "event",
//  "api_version": "2023-08-16",
//  "created": 1697098231,
//  "data": {
//                    "object": {
//                        "id": "cs_test_b1ohwjH2cFqWnAS5PHTQJEBsJfuIB0usLDZ7lwtIZvZdYxCrB5ZBvIp3nZ",
//      "object": "checkout.session",
//      "after_expiration": null,
//      "allow_promotion_codes": null,
//      "amount_subtotal": 6600,
//      "amount_total": 6600,
//      "automatic_tax": {
//                            "enabled": false,
//        "status": null
//      },
//      "billing_address_collection": null,
//      "cancel_url": "http://localhost:8000/purchase/cancel",
//      "client_reference_id": null,
//      "consent": null,
//      "consent_collection": null,
//      "created": 1697011830,
//      "currency": "usd",
//      "currency_conversion": null,
//      "custom_fields": [
//                        ],
//      "custom_text": {
//                            "shipping_address": null,
//        "submit": null,
//        "terms_of_service_acceptance": null
//      },
//      "customer": null,
//      "customer_creation": "if_required",
//      "customer_details": null,
//      "customer_email": null,
//      "expires_at": 1697098230,
//      "invoice": null,
//      "invoice_creation": {
//                            "enabled": false,
//        "invoice_data": {
//                                "account_tax_ids": null,
//          "custom_fields": null,
//          "description": null,
//          "footer": null,
//          "metadata": {
//                                },
//          "rendering_options": null
//        }
//      },
//      "livemode": false,
//      "locale": null,
//      "metadata": {
//                        },
//      "mode": "payment",
//      "payment_intent": null,
//      "payment_link": null,
//      "payment_method_collection": "if_required",
//      "payment_method_configuration_details": null,
//      "payment_method_options": {
//                        },
//      "payment_method_types": [
//                            "card"
//                        ],
//      "payment_status": "unpaid",
//      "phone_number_collection": {
//                            "enabled": false
//      },
//      "recovered_from": null,
//      "setup_intent": null,
//      "shipping_address_collection": null,
//      "shipping_cost": null,
//      "shipping_details": null,
//      "shipping_options": [
//                        ],
//      "status": "expired",
//      "submit_type": null,
//      "subscription": null,
//      "success_url": "http://localhost:8000/purchase/success",
//      "total_details": {
//                            "amount_discount": 0,
//        "amount_shipping": 0,
//        "amount_tax": 0
//      },
//      "url": null
//    }
//  },
//  "livemode": false,
//  "pending_webhooks": 2,
//  "request": {
//                    "id": null,
//    "idempotency_key": null
//  },
//  "type": "checkout.session.expired"
//}
            break;
            // ... handle other event types
            default:
                echo 'Received unknown event type ' . $event->type;
        }

        return response('', 200);
    }
}
