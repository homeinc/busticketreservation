<?php

namespace App\Http\Controllers;

use App\Http\Requests\FindToursRequest;
use App\Models\City;
use App\Models\Tour;
use Illuminate\Support\Facades\DB;

class FindToursController extends Controller
{
    public function findTours(FindToursRequest $request)
    {
//        $tours = Tour::with('trips')
//            ->whereHas('trips', function ($query) use ($request) {
//                $query->where('departure_date', '>', date('Y-m-d', strtotime($request->from_date)))
//                    ->where('arrival_date', '<', date('Y-m-d', strtotime($request->to_date)));
//            })
//            ->whereHas('departure', function ($query) use ($request) {
//                $query->where('id', $request->departure_station);
//            })
//            ->whereHas('destination', function ($query) use ($request) {
//                $query->where('id', $request->destination_station);
//            })
//            ->get();

//        $tours = DB::table('tours')
//            ->select('tours.id AS tour_id', 'tours.title', 'tours.description', 'trips.id AS trip_id')
//            ->leftJoin('trips', 'tours.id', '=', 'trips.tour_id')
//            ->leftJoin('seats', 'trips.id', '=', 'seats.trip_id')
//            ->where('seats.status', '!=', 'Booked') // Exclude booked seats
//            ->where('seats.status', '!=', 'Reserved') // Exclude reserved seats
//            ->groupBy('tours.id', 'tours.title', 'tours.description', 'trips.id')
//            ->get();
        $ticket_count = $request->ticket_count;
        $tours = Tour::select('tours.id as tour_id', 'tours.title', 'tours.description', 'trips.id as trip_id',
                   'buses.capacity as capacity',
                   'trips.departure_date',
                   'trips.arrival_date',
                   'tours.default_price as price',
                   'tours.company' ,
                   'tours.distance',
                   'tours.logo',
                   'tours.duration_hours as hours',
                   'tours.duration_minutes as minutes',
                   's1.name as departure_station',
                   's2.name as arrival_station' )
            ->leftJoin('trips', 'tours.id', '=', 'trips.tour_id')
            ->leftJoin('seats', 'trips.id', '=', 'seats.trip_id')
            ->leftJoin('buses', 'trips.bus_id', '=', 'buses.id')
            ->leftJoin('stations as s1', 's1.id', '=', 'tours.departure_id')
            ->leftJoin('stations as s2', 's2.id', '=', 'tours.arrival_id')
            ->leftjoin ('cities as dep_city','dep_city.id','s1.city_id')
            ->leftjoin ('cities as des_city','des_city.id','s2.city_id')
            ->selectRaw('SUM(CASE WHEN seats.status <> "Free" THEN 1 ELSE 0 END) as occupied_seats')
            ->where('dep_city.id', $request->departure_station)
            ->where('des_city.id',$request->destination_station)
            ->groupBy('tours.id', 'tours.title', 'tours.description', 'trips.id', 'buses.capacity')
            ->havingRaw ('capacity - occupied_seats > ? ',[$ticket_count])
            ->paginate();

        return view('findtours.index', compact('tours', 'ticket_count'));
    }
}
