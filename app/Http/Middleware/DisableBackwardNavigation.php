<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class DisableBackwardNavigation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
//    public function handle(Request $request, Closure $next): Response
//    {
//        $response = $next($request);
//        $response->header('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
//        $response->header('Pragma', 'no-cache');
//        $response->header('Expires','Fri, 01 Jan 1990 00:00:00 GMT');
//        return $response;
//    }

    public function handle(Request $request, Closure $next)
    {

        $response = $next($request);
        $response->header('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $response->header('Pragma', 'no-cache');
        $response->header('Expires','Fri, 01 Jan 1990 00:00:00 GMT');
//        if (!session()->has('canNavigate')) {
//            abort(401);
//        }

        // Allow navigation if 'canNavigate' is set
        return $response;
    }
}
