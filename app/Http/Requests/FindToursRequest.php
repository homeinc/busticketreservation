<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FindToursRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'departure_station' => ['string', 'required'],
            'destination_station' => ['string', 'required'],
            'from_date' => ['date', 'required'],
            'to_date'=> ['date', 'required'],
            'ticket_count' => ['integer', 'required']
        ];
    }
}
