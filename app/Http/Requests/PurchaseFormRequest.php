<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PurchaseFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return !empty(auth()->id());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'full_name' => ['required', 'string'],
            'email' => ['email', 'required'],
            'social_security' => ['string', 'required', 'min:10', 'max:14'],
            'credit_card' =>['string', 'required', 'size:16'],
            'expire_date' =>['required', 'date'],
            'ccv' => ['required', 'string', 'size:3']
        ];
    }
}
