<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Tour extends Model
{
    use HasFactory, Sluggable;

    protected $guarded=['id'];
    public function trips()
    {
        return $this->hasMany(Trip::class);
    }

    public function departure()
    {
        return $this->belongsTo(Station::class, 'departure_id');
    }


    public function destination()
    {
        return $this->belongsTo(Station::class, 'destination_id');
    }



    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title',
            ]
        ];
    }

    protected function detaultPrice() :Attribute
    {
        return Attribute::make(
            get: fn($price) => $price / 100,
            set : fn($price) => $price * 100
        );
    }
}
