<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;

    protected $guarded =[];
    public function stations()
    {
        return $this->hasManyThrough(Station::class, City::class);
    }
}
