<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Trip extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    public function bus()
    {
        return $this->belongsTo(Bus::class);
    }

    public function seats()
    {
        return $this->hasMany(Seat::class);
    }

    public function tour() : BelongsTo
    {
        return $this->belongsTo(Tour::class);
    }

    public function price() :Attribute
    {
        return Attribute::make(
            get: fn($price) => $price / 100,
            set : fn($price) => $price * 100
        );
    }


}
