<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Station extends Model
{
    use HasFactory;
    use \Znck\Eloquent\Traits\BelongsToThrough;

    protected $fillable=['city_id', 'name', 'abbreviation', 'latitude', 'longitude'];
    public function country()
    {
        return $this->belongsToThrough(Country::class, City::class);
    }


    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
