<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;
    protected $guarded=[];
    public function  tours()
    {
        return $this->hasManyThrough(Tour::class, Station::class, 'city_id', 'departure_id');
    }
}
