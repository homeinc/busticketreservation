<?php

namespace App\services;

use App\Models\Payment;
use App\Models\Seat;
use App\Models\Trip;
use App\Models\User;
use http\Exception\InvalidArgumentException;
use Illuminate\Support\Facades\DB;

class   TicketService
{
    public function bookNewSeat($trip_id, $number_of_tickets): iterable
    {
        $trip = Trip::with('tour')->where('id', $trip_id)->first();
        if (!$trip) {
            return new \Exception('Invalid Tour id');
        }
        $total_ticket_count = $trip->bus->capacity;
        $purchased_ticket_count = $trip->seats->count();

        $user = User::find(auth()->id())->first();
        $pendingTickets = $user->seats;
        if ($pendingTickets ) {
            foreach ($pendingTickets as $pendingTicket) {
                if ($pendingTicket->status == 'Pending') {
                    $pendingTicket->delete();
                }
            }
            $trip->load('seats');
            $purchased_ticket_count = $trip->seats->count();
        }
        if ($purchased_ticket_count + $number_of_tickets <= $total_ticket_count) {
                for ($i = 0; $i < $number_of_tickets; $i++) {
                    $newSeat = Seat::create([
                        'user_id' => $user->id,
                        'trip_id' => $trip_id,
                        'seat' => $this->generateSeatNames($purchased_ticket_count),
                        'price' => $trip->tour->default_price,
                        'status' => 'Pending',
                        'tax' => '1'
                    ]);

                    $trip->load('seats');
                    $purchased_ticket_count = $trip->seats->count();
                    yield ($newSeat);
                }
            }
    }

    function generateSeatNames(int $occupied_seats): string
    {
        $letters = ['A', 'B', 'C', 'D'];
        $first_vacant_seat = $occupied_seats + 1;
        $mod = $first_vacant_seat % count($letters);
        $row = ceil($first_vacant_seat / count($letters));
        if ($mod == 0)
            $mod = count($letters) - 1;
        else
            $mod = $mod - 1;
        return $row . $letters[$mod];
    }

    function confirmBuyingTickets(string $session_id)
    {
        $seats = Seat::where('user_id', auth()->id())->where('status', 'Pending')->get();
      try {
          DB::transaction(function () use ($seats, $session_id) {
              $data = [];
              foreach ($seats as $seat) {
                  $seat->status = 'Purchased';
                  $seat->updated_at = date('Y-m-d H:i:s');
                  $seat->save();
                  $data[] = $seat->only(['trip_id', 'seat', 'price', 'status']);
              }

              Payment::create([
                  'user_id' => auth()->id(),
                  'amount' => $seats->first()->price * $seats->count(),
                  'session_id' => $session_id,
                  'data' => json_encode(['data' => $data])
              ]);
          });
      }catch(\Exception $e) {
          throw new InvalidArgumentException('Invalid argument provided');
      }
    }

}
