<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Bus;
use App\Models\City;
use App\Models\Country;
use App\Models\Station;
use App\Models\Tour;
use App\Models\Trip;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {

        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('countries')->truncate();
        DB::table('cities')->truncate();
        DB::table('buses')->truncate();
        DB::table('tours')->truncate();
        DB::table('trips')->truncate();
        DB::table('seats')->truncate();
        DB::table('stations')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        Country::factory(3)->create();
        City::factory(8)->create();
        Bus::factory(2)->create();
        Station::factory(10)->create();
        Tour::factory(100)->create();
        Trip::factory(400)->create();
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
