<?php

namespace Database\Seeders;

use App\Models\Trip;
use Database\Factories\TripFactory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TripsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Trip::factory()->create();
    }
}
