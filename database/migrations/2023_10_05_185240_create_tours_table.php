<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tours', function (Blueprint $table) {
            $table->id();
            $table->foreignId('departure_id')->references('id')->on('stations');
            $table->foreignId('arrival_id')->references('id')->on('stations');
            $table->string('slug')->unique();
            $table->string('company');
            $table->string('title');
            $table->string('logo');
            $table->integer('distance');
            $table->string('abbreviation');
            $table->unsignedTinyInteger('duration_hours');
            $table->unsignedSmallInteger('duration_minutes');
            $table->integer('default_price');
            $table->text('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tours');
    }
};
