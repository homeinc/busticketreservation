<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('seats', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreignId('trip_id')->references('id')->on('trips');
            $table->string('seat');
            $table->integer('price');
            $table->integer('tax');
            $table->enum('status', [ 'Available', 'Booked',  'Reserved', 'Purchased', 'Canceled', 'Returned' , 'Pending']);
            $table->unique(['trip_id', 'seat']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('seats');
    }
};
