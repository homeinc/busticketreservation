<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->id();
            $table->foreignId('bus_id')->references('id')->on('buses');
            $table->foreignId('tour_id')->references('id')->on('tours');
            $table->dateTime('departure_date');
            $table->dateTime('arrival_date');
            $table->unsignedSmallInteger('active')->default(1);
            $table->enum('status', ['STATUS_OK', 'STATUS_DELAYED',  'STATUS_ONTIME',  'STATUS_CANCELED',  'STATUS_LATE']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('trips');
    }
};
