<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->references('id')->on('users')->onDelete('cascade');
<<<<<<< HEAD
            $table->string('first_name');
=======
            $table->string('name');
>>>>>>> dd84405 (Adding some migrations)
            $table->string('last_name');
            $table->date('dob');
            $table->smallInteger('gender');
            $table->string('social_security');
            $table->string('photo')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_details');
    }


};
