<?php

namespace Database\Factories;

use App\Models\Station;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Tour>
 */
class TourFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $busCompanies = [
            'CTI' => [
                'company' => 'Cline Tours Inc.',
                'abbreviation' => 'CTI'
            ],
            'VIT' => [
                'company' => 'Village Travel',
                'abbreviation' => 'VIT'
            ],
            'BLC' => [
                'company' => 'BLUE LAKES CHARTERS & TOURS',
                'abbreviation' => 'BLC',
            ],
            'SSF'=> [
                'company' => 'Storer San Francisco',
                'abbreviation' => 'SSF',
            ],
            'DAT' =>[
                'company' => 'DATTCO Inc.',
                'abbreviation' => 'DAT',
            ],
            'LBL' =>[
                'company' => 'Lamers Bus Lines Inc.',
                'abbreviation' => 'LBL',
            ],
            'PCC' =>[
                'company' => 'Peoria Charter Coach',
                'abbreviation' => 'PCC',
            ],
            'NCT' =>[
                'company' => 'NorthEast Charter & Tour',
                'abbreviation' => 'NCT',
            ],
            'CBL' =>[
                'company' => 'CYR Bus Line',
                'abbreviation' => 'CBL',
            ],
            'SBC' => [
                'company' => 'Starr Bus Charter & Tours',
                'abbreviation' => 'SBC',
            ],
            'HAJ' =>[
                'company' => 'Hampton Jitney',
                'abbreviation' => 'HAJ',
            ],
            'BCI'=> [
                'company' => 'Brown Coach Inc',
                'abbreviation' => 'BCI',
            ],
            'HTI' =>[
                'company' => 'Holiday Tours Inc',
                'abbreviation' => 'HTI',
            ],
            'TBL' =>[
                'company' => 'Trans-Bridge Lines Inc.',
                'abbreviation' => 'TBL',
            ],
            'GLT' =>[
                'company' => 'Gray Line Tennessee',
                'abbreviation' => 'GLT',
            ],
            'SSC' =>[
                'company' => 'Star Shuttle & Charter',
                'abbreviation' => 'SSC',
            ],
            'DTI' =>[
                'company' => 'DC Trails Inc.',
                'abbreviation' => 'DTI',
            ],
            'SCO' =>[
                'company' => 'Starline Collection',
                'abbreviation' => 'SCO',
            ],
            'GHD' => [
                'company' => 'GrayHound',
                'abbreviation' => 'GHD',
            ],
            'PEP'=>[
                'company' => 'Peter-Pan',
                'abbreviation' => 'PEP',
            ],

        ];

        $key = array_rand($busCompanies);
        return [
            'company' => $busCompanies[$key]['company'],
            'title'=>$this->faker->sentence,
            'departure_id' => Station::inRandomOrder()->first()->id,
            'destination_id' => Station::inRandomOrder()->first()->id,
            'logo' => $this->faker->imageUrl(48,48),
            'distance' => $this->faker->randomFloat(3, 0, 1) * 1000,
            'abbreviation'=>$busCompanies[$key]['abbreviation'],
            'duration_hours' => $this->faker->numberBetween(2, 12),
            'duration_minutes' => $this->faker->numberBetween(0, 5) * 10,
            'default_price' => $this->faker->randomNumber(2),
            'description' => $this->faker->text()
        ];
    }
}
