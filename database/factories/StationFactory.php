<?php

namespace Database\Factories;

use App\Models\City;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class StationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'city_id' => City::inRandomOrder()->first()->id,
            'name' => $this->faker->word,
            'abbreviation' => $this->faker->randomLetter() . $this->faker->randomLetter() .$this->faker->randomLetter(),
            'latitude' => $this->faker->randomFloat(5, 1, 2),
            'longitude' => $this->faker->randomFloat(5, 1, 2),
        ];
    }
}
