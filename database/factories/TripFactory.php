<?php

namespace Database\Factories;

use App\Models\Bus;
use App\Models\Tour;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Trip>
 */
class TripFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'bus_id' => Bus::inRandomOrder()->first()->id,
            'tour_id' => Tour::inRandomOrder()->first()->id,
            'departure_date' =>$this->faker->dateTimeBetween('+2 days', '+3 days'),
            'arrival_date' => $this->faker->dateTimeBetween('+2 days', '+3 days'),
            'status' =>   'STATUS_OK',
        ];
    }
}
