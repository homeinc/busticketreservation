<?php

use App\Http\Controllers\FindToursController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\PurchaseFormController;
use App\Http\Controllers\PurchaseTicketsController;
use App\Http\Middleware\DisableBackwardNavigation;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';

Route::GET('/', [HomeController::class, 'index'])->name('home');
Route::GET('/findtours', [FindToursController::class, 'findTours'])->name('findtours')->middleware(DisableBackwardNavigation::class);
Route::GET('/purchaseform', [PurchaseFormController::class, 'index'])->name('purchaseform.index')->middleware(DisableBackwardNavigation::class);;
Route::POST('/purchaseform', [PurchaseFormController::class, 'store'])->name('purchaseform.store')->middleware(DisableBackwardNavigation::class);;

Route::prefix('/purchase')->group(function () {
    Route::POST('/', [PurchaseTicketsController::class, 'store'])->name('purchase.store');
    Route::GET('/success', [PurchaseTicketsController::class, 'success'])->name('purchase.success');
    Route::GET('/cancel', [PurchaseTicketsController::class, 'cancel'])->name('purchase.cancel');
    Route::POST('/webhook', [PurchaseTicketsController::class, 'webhook'])->name('purchase.webhook');
});
